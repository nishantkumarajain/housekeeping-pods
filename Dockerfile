FROM alpine:3.7
WORKDIR /housekeeping
RUN wget https://storage.googleapis.com/kubernetes-release/release/v1.17.3/bin/linux/amd64/kubectl && chmod +x kubectl && mv kubectl /usr/local/bin
COPY kubeconfig.yml /housekeeping
COPY remove-evicted-pods.sh /housekeeping
RUN chmod +x /housekeeping/remove-evicted-pods.sh
ENV KUBECONFIG=/housekeeping/kubeconfig.yml
ENTRYPOINT [ "./remove-evicted-pods.sh" ]