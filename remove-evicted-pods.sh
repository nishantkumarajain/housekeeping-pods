#!/bin/sh
kubectl get pods --all-namespaces | grep -i evicted | awk '{print $1 " " $2}' | while read NS POD
do
kubectl delete pod $POD -n $NS
done